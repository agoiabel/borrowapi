<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BorrowApi</title>
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
	<div class="login-page container">
		
		<div class="header">
			<div>
				<div class="logo"><img src="{{ asset('./images/logo-icon.png') }}" alt=""></div>
				<div class="page-title">Sign In</div>
			</div>
		</div>

		<div>

			<form class="login card" action="{{ route('web-auth.store') }}" method="POST">
				@csrf
				<div class="form-group">
					<label for="email" class="label-control">Email Address</label>
					<input type="email" name="email" class="form-control">
				</div>

				<div class="form-group">
					<label for="password" class="label-control">Password</label>
					<input type="password" name="password" class="form-control">
				</div>
				<button class="button">Sign In</button>
			</form>

		</div>


	</div>
</body>
</html>