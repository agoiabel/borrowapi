<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>To All</title>
    <style>
        
        @import url('https://fonts.googleapis.com/css?family=Ubuntu');
        
        html body {
            font-family: 'Ubuntu', sans-serif;
            background: #efefef !important;
            width: 100%; 
        }

        h1{
            font-size: 2em;
            color: #0BC773;
        }
        
        h2 {
            font-size: 1.2em;
            color: #fff;
        }
        
        .btn{
            padding: 16px 25px;
            border:2px solid  #fff;
            text-decoration: none;
            
        }
    
        .footer {
            text-align: center;
        }
        
        .footer a {
            display: inline;
            text-decoration: none;
        }
        
        a {
            text-decoration: none;
        }
        
        p {
            color: #111;
            font-size: 1em;
        }
        
        @media only screen and (max-device-width: 780px){
          .mobile {
              padding-top: 35px;
            }
        }
    </style>
</head>
<body style="background: #efefef !important;  padding-top: 50px !important;">

    <center>

        <!--Body-->
        <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background: #fff; padding: 70px;">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#">
                                    <img style="text-align: center;" src="{{ asset('images/logo-icon.png') }}">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table border="0" class="mobile" style="padding-top: 5px;">
                        <tr>
                            <td style="width: 70%;">
                                <h1 style="font-size: 2em; color: #0BC773;">Loan Application Notification</h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> 
        
        <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding: 0px;">
            <tr style="border-spacing: 3em;"> <!-- call to action-->
                <td style="padding: 20px 70px; background-color: linear-gradient(-135deg, #6A60EE 0%, #56EDFF 100%)">
                    <h2 style="color: #fff; margin-top:50px; padding-bottom:20px;">Hi {{ $borrower->firstname }} {{ $borrower->lastname }},</h2>
						
					<p style="font-size: 1em; color: #fff; padding-bottom: 40px;">
						Your loan application was successful.
					</p>

                    <p style="font-size: 1em; color: #fff; padding-bottom: 40px;">
                        We will give you a loan of {{ number_format($loan->amount->amount, 2, ',', '.') }} which you are going to use to pay {{ number_format($refundAmount, 2, ',', '.') }} on the {{ $loan->date_to_refund }}.

                        <a href="{{route('web-loan.chargeCard', ['loan_slug' => $loan->slug])}}">Linkjsjsjsjsjs</a>
                    </p>
        
                    <p style=" color: #fff; padding-top: 40px;font-size: 1em;">
                        If you did not apply for a loan on Borrow and got this email in error, kindly ignore. Someone probably just typed the wrong email address.<br><br>
                        Have a lovely day,<br>
                        The Borrow Team.
                    </p>
                </td>
            </tr><!--end here-->
        </table>

    </center>
    </body>
</html>