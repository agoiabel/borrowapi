<nav class="top-navigation">
    <div class="container navigation">

        <div class="logo"><img src="{{ asset('./images/logo-icon.png') }}" alt=""></div>

        <div class="nav-right-menu">

			<div id="nav-right-menu" class="nav-menu">
				<div class="name"> {{ Auth::user()->firstname }} {{ Auth::user()->lastname }} </div>
	        	<div class="downdown-icon"><i class="fa fa-caret-down"></i></div>
			</div>        	


        	<ul class="dropdown-menu card" id="dropdown-menu">
                
                <li class="dropdown-menu__item">
                    <a href="{{ route('web-loan.loan-due-threee-days-from-now') }}">
                        <i class="fa fa-at"></i> Loan dues 3 days
                    </a>
                </li>

        		<li class="dropdown-menu__item">
        			<a href="{{ route('web-borrow.awaiting') }}">
        				<i class="fa fa-history"></i> Loan awaiters
        			</a>
        		</li>

        		<li class="dropdown-menu__item">
        			<a href="{{ route('web-loan.readyToPayout') }}">
        				<i class="fa fa-location-arrow"></i> Loan payout
        			</a>
        		</li>

        		<li class="dropdown-menu__item">
        			<a href="#">
        				<i class="fa fa-power-off"></i> Logout
        			</a>
        		</li>
                
        	</ul>

        </div>
    </div>
</nav>