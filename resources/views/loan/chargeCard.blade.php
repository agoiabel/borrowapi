@extends('shared.layout')
@section('content')
	<div class="login-page container">
		
		<div class="header">
			<div>
				<div class="logo"><img src="{{ asset('./images/logo-icon.png') }}" alt=""></div>
			</div>
		</div>

		@if ($flash = session('message'))
			<div class="alert alert-success" role="alert">
				{{ $flash }}
			</div>
		@endif

		<div>
			<ul class="instruction card">
				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem dolores harum veniam necessitatibus voluptatibus veritatis sit eveniet possimus accusamus nobis aliquam vel magnam perspiciatis nam, nulla repudiandae unde quae ipsam.					
				</li>

				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem dolores harum veniam necessitatibus voluptatibus veritatis sit eveniet possimus accusamus nobis aliquam vel magnam perspiciatis nam, nulla repudiandae unde quae ipsam.					
				</li>

				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem dolores harum veniam necessitatibus voluptatibus veritatis sit eveniet possimus accusamus nobis aliquam vel magnam perspiciatis nam, nulla repudiandae unde quae ipsam.					
				</li>

				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem dolores harum veniam necessitatibus voluptatibus veritatis sit eveniet possimus accusamus nobis aliquam vel magnam perspiciatis nam, nulla repudiandae unde quae ipsam.					
				</li>

				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem dolores harum veniam necessitatibus voluptatibus veritatis sit eveniet possimus accusamus nobis aliquam vel magnam perspiciatis nam, nulla repudiandae unde quae ipsam.					
				</li>
			</ul>

			<button class="button" onclick="payWithPaystack()">Charge my card</button>
		</div>
	</div>
@endsection
@section('footer')
	<script src="https://js.paystack.co/v1/inline.js"></script>
	<script>
		const loan = {!! $loan !!};
		const transaction = {!! $transaction !!};
		const base_url = document.head.querySelector("[property=url]").content;

		const payWithPaystack = () => {

			console.dir('paystack');

			const handler = PaystackPop.setup({
				key: 'pk_test_c6107f2bff6d8a2d211f6cce9b9067f612f29a14',
				email: loan.borrower.email,
				amount: transaction.amount * 100,
				ref: transaction.reference,
				firstname: loan.borrower.firstname,
				lastname: loan.borrower.lastname,
				callback: response => {
					window.location = base_url+'/loan/confirmCard/'+transaction.reference+'/'+response.reference;
				},
				onClose: function() {
					alert('window closed');
				}
			});
			handler.openIframe();
		}
	</script>
@endsection
