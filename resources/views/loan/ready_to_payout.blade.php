@extends('shared.layout')
@section('content')

@include('shared._nav')

<div class="loan-awaiters container" ng-app="app" ng-controller="appCtrl">

    <div class="loan-awaiter card" ng-repeat="(key, user) in users">
        <div class="loan-details">
            <div class="amount-applied-date">
                <div class="loan-amount">
                    <i class="fab fa-bitcoin"></i> @{{ user.active_loan.amount.amount }}
                </div>
                <div class="applied-date">@{{ user.active_loan.applied_date }}</div>
            </div>

            <div class="borrower-name">
                @{{ user.active_loan.borrower.firstname }} @{{ user.active_loan.borrower.lastname }} | @{{ user.active_loan.borrower.gender }}
            </div>
        </div>
        <div class="complete-verification verification-status">
            <div ng-class="getValidityStatusFor(user.active_loan)" id="toggle" ng-click="toogleValidityFor(key, user.active_loan)">
                <div class="switch"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('/js/angular.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script>
    const users = {!! $users !!};

    console.dir(users);

    (function () {
        const app = angular.module('app', []);

        app.controller('appCtrl', function ($scope, $http) {

            $scope.users = users;

            /**
             * Update the validity status
             * @param  event
             * @param  borrowerLoanValidity 
             * @return
             */
            $scope.toogleValidityFor = async (key, loan) => {
                const clickedElement = event.currentTarget;

                clickedElement.classList.contains('active') ? clickedElement.classList.remove('active') : clickedElement.classList.add('active');

                const updateResponse = await $http({
                    url: "{!! route('web-loan.payout') !!}", 
                    method: 'POST',
                    params: {
                        loan_id: loan.id
                    }
                 });

                if (updateResponse.data.status === 200) {
                    $scope.loans.splice(key, 1);
                }
            }

            /**
             * Get the validity status and return appropriate class
             * 
             * @param  borrowerLoanValidity 
             * @return 
             */
            $scope.getValidityStatusFor = loan => {
                if (loan.status === 4) {
                    return 'toggle active';
                }
                return 'toggle';
            }


        });

    })();
</script>
@endsection
