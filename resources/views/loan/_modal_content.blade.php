<!-- modal -->
<div class="modal" id="IDENTITY">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('IDENTITY')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>Name</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>
							<img src="{!! asset('@{{ borrowerLoanValidity.payload }}') !!}" alt="">
						</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="PHONE_NUMBER">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('PHONE_NUMBER')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>Phone number</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="BVN">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('BVN')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>BVN</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="EMPLOYER">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('EMPLOYER')">&times;</span>
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>EMPLOYER</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>		
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="GUARANTOR">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('GUARANTOR')">&times;</span>
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>GUARANTOR</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>		
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="ACCOUNT_DETAILS">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('ACCOUNT_DETAILS')">&times;</span>
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>ACCOUNT_DETAILS</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>		
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="SALARY_RANGE">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('SALARY_RANGE')">&times;</span>
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>SALARY_RANGE</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>		
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" id="ADDRESS">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('ADDRESS')">&times;</span>
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>ADDRESS</td>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<td>@{{ borrowerLoanValidity.payload }}</td>
					</tr>		
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- modal -->
<div class="modal" id="CONTACT">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('CONTACT')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>Name</td>
						<td>Number</td>						
					</tr>
				</thead>
				<tbody>
					<tr class="" ng-repeat="(key, contact) in contacts">
						<td style="width: 100%">@{{ contact.name }}</td>
						<td style="width: 100%">@{{ contact.number }}</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- modal -->
<div class="modal" id="CALL_LOG">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('CALL_LOG')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>Phone number</td>
						<td>Call Day</td>
						<td>Call Type</td>						
						<td>Call Duration</td>						
					</tr>
				</thead>
				<tbody>
					<tr class="" ng-repeat="(key, call_log) in call_logs">
						<td style="width: 100%">@{{ call_log.phoneNumber }}</td>
						<td style="width: 100%">@{{ call_log.callDayTime }}</td>
						<td style="width: 100%">@{{ call_log.callType }}</td>
						<td style="width: 100%">@{{ call_log.callDuration }}</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- modal -->
<div class="modal" id="SMS">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('SMS')">&times;</span>			
		</div>
		
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<td>Sender</td>
						<td>Body</td>
						<td>Type</td>						
					</tr>
				</thead>
				<tbody>
					<tr class="" ng-repeat="(key, message) in messages">
						<td style="width: 100%">@{{ message.address }}</td>
						<td style="width: 100%">@{{ message.body }}</td>
						<td style="width: 100%">@{{ getSmsTypeFrom(message.type) }}</td>
					</tr>					
				</tbody>
			</table>
		</div>
		
	</div>
</div>


<!-- modal -->
<div class="modal" id="TRANSACTION">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('TRANSACTION')">&times;</span>		
		</div>
		
		<div class="body">
			<form class="transaction-add" ng-submit="createTransactionHandler()">
				<div class="form-group">
					<input type="number" name="amount" class="form-control" ng-model="transactionFormData.amount">
				</div>
				<button class="button">Create {{ $loan->borrower->firstname }} {{ $loan->borrower->lastname }} Transaction</button>
			</form>

			<table class="table">
				<thead>
					<tr>
						<td>Transaction Type</td>
						<td>Amount</td>
						<td>Date</td>						
						<td>Status</td>
					</tr>
				</thead>
				<tbody>
					<tr class="" ng-repeat="(key, transaction) in transactions">
						<td style="width: 100%">@{{ transaction.type }}</td>
						<td style="width: 100%">@{{ transaction.amount }}</td>
						<td style="width: 100%">@{{ transaction.created_at }}</td>
						<td style="width: 100%">@{{ transaction.confirmed }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>

<!-- modal -->
<div class="modal" id="TRANSACTION-ADD">
	<div class="modal-content card">
		<div class="header">
			<span class="close" id="close" ng-click="closeModal('TRANSACTION-ADD')">&times;</span>			
		</div>
		
		<form class="body" ng-submit="createTransactionHandler()" name="transactionForm">
			<input type="hidden" name="loan_id" value="{{$loan->id}}" model="transaction.loan_id">
			<input type="hidden" name="user_id" value="{{$loan->borrower->id}}" model="transaction.user_id">
			<input type="hidden" name="type" value="LOAN_REBURSEMENT_PARTIAL" model="transaction.type">

			<div class="form-group">
				<input type="number" name="amount" class="form-control" model="transaction.amount">
			</div>

			<button class="button">Create</button>
		</form>
	</div>
</div>