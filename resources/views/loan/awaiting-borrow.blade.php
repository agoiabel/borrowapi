@extends('shared.layout')
@section('content')
	
	@include('shared._nav')

	<div class="loan-awaiters container">
		@foreach ($users as $user)
			<div class="loan-awaiter card">
				<div class="loan-details">
					<div class="amount-applied-date">
						<div class="loan-amount">
							<i class="fab fa-bitcoin"></i> {{ $user->activeLoan->amount->amount }}
						</div>
						<div class="applied-date">{{ $user->activeLoan->applied_date }}</div>
					</div>

					<div class="borrower-name">
						{{ $user->activeLoan->borrower->firstname }} {{ $user->activeLoan->borrower->lastname }} | {{ $user->activeLoan->borrower->gender }}
					</div>
				</div>
				<a class="more-detail" href="{{ route('web-borrow.profile', ['loan_slug' => $user->activeLoan->slug]) }}">
					<i class="fa fa-folder-open"></i>
				</a>
			</div>
		@endforeach
	</div>
	
@endsection
