@extends('shared.layout')
@section('content')
	
	@include('shared._nav')

	<div class="loan-awaiters container">
		@foreach ($loans as $loan)
			<div class="loan-awaiter card">
				<div class="loan-details">
					<div class="amount-applied-date">
						<div class="loan-amount">
							<i class="fab fa-bitcoin"></i> {{ $loan->amount->amount }}
						</div>
						<div class="applied-date">{{ $loan->date_to_refund->diffForHumans() }}</div>
					</div>

					<div class="borrower-name">
						{{ $loan->borrower->firstname }} {{ $loan->borrower->lastname }} | {{ $loan->borrower->gender }}
					</div>
				</div>
				<a class="more-detail" href="{{ route('web-borrow.pay-back-profile', ['loan_slug' => $loan->slug]) }}">
					<i class="fa fa-folder-open"></i>
				</a>
			</div>
		@endforeach
	</div>
	
@endsection
