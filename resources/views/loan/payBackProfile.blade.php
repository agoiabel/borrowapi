@extends('shared.layout')
@section('content')

<div ng-app="app" ng-controller="appCtrl">
    
    @include('shared._nav')

    <div class="profile container">
        <div class="profile-header card">
            <div class="borrower-name">{{ $loan->borrower->firstname }} {{ $loan->borrower->lastname }}</div>
            <div class="borrower-email">{{ $loan->borrower->email }}</div>
            <div class="decision-buttons">
                <div class="decision warning-decision" ng-click="openTransactionFor({{ $loan }})">
                    TRANSACTION
                </div>
                <div class="decision acceptance-decision" ng-click="chargeCard({{ $loan }})">
                    CHARGE CARD
                </div>
                <div class="decision alert-success" ng-click="completeTransaction({{ $loan }})">
                    COMPLETE TRANSACTION
                </div>
            </div>
        </div>

        <div class="loan-approval-point">
            <div class="amount-applied card">
                <div class="header">Amount Borrowed</div>
                <div class="body">NGN {{ $loan->amount->amount }}</div>
            </div>
            <div class="verification-point card">
                <div class="header">Total Refund</div>
                <div class="body">4</div>
            </div>
        </div>
    </div>
    <div class="verifications container card">

        @include('loan._modal_content');
    
        <div class="header"> Verifications </div>
        <div class="body">
            
            <div class="verification" ng-repeat="(key, borrowerLoanValidity) in borrowerLoanValidities">
                <div class="header">
                    <div class="verification-title">
                        @{{ borrowerLoanValidity.validity.name }}   
                    </div>
                    <div class="verification-status">
                        <div ng-class="getValidityStatusFor(borrowerLoanValidity)" id="toggle" ng-click="toogleValidityFor($event, borrowerLoanValidity)">
                            <div class="switch"></div>
                        </div>
                    </div>
                </div>

                <div class="body">
                    @{{ borrowerLoanValidity.validity.description }}
                </div>
                <div class="footer">
                    <i class="fa fa-folder-open" id="openModal" ng-click="displayPayloadFor(borrowerLoanValidity)"></i>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('/js/angular.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script>
    const borrowerLoanValidities = {!! $loan->borrowerLoanValidities !!};
    const loan = {!! $loan !!};

    (function () {
        const app = angular.module('app', []);

        app.controller('appCtrl', function ($scope, $http) {

            $scope.borrowerLoanValidities = borrowerLoanValidities;
            $scope.transactions = [];

            $scope.transactionFormData = {
                loan_id: loan.id,
                user_id: loan.borrower.id,
                type: 'LOAN_REBURSEMENT_PARTIAL',
                amount: ''
            };

            /**
             * Update the validity status
             * @param  event
             * @param  borrowerLoanValidity 
             * @return
             */
            $scope.toogleValidityFor = async (event, borrowerLoanValidity) => {
                const clickedElement = event.currentTarget;
                const updateResponse = await $http({
                    url: "{!! route('web-borrow-update.validity') !!}", 
                    method: 'POST',
                    params: {
                        borrower_loan_validity_id: borrowerLoanValidity.id
                    }
                 });

                if (updateResponse.data.status === 200) {
                    clickedElement.classList.contains('active') ? clickedElement.classList.remove('active') : clickedElement.classList.add('active');
                }
            }

            /**
             * Get the validity status and return appropriate class
             * 
             * @param  borrowerLoanValidity 
             * @return 
             */
            $scope.getValidityStatusFor = borrowerLoanValidity => {
                if (borrowerLoanValidity.is_confirmed === 1) {
                    return 'toggle active';
                }
                return 'toggle';
            }

            /**
             * Display appropriate payload for the modal
             * 
             * @param  borrowerLoanValidity 
             * @return                      
             */
            $scope.displayPayloadFor = borrowerLoanValidity => {
                
                $scope.borrowerLoanValidity = borrowerLoanValidity;
                let modal = document.getElementById(borrowerLoanValidity.validity.name);

                if (borrowerLoanValidity.validity.name == 'CONTACT') {
                    $scope.contacts = JSON.parse(borrowerLoanValidity.payload);
                }

                if (borrowerLoanValidity.validity.name == 'CALL_LOG') {
                    $scope.call_logs = JSON.parse(borrowerLoanValidity.payload);
                }

                if (borrowerLoanValidity.validity.name == 'SMS') {
                    $scope.messages = JSON.parse(borrowerLoanValidity.payload);
                }


                modal.classList.contains('hide') ? modal.classList.remove('hide') : modal.classList.add('show');
            }

            /**
             * Close appropriate modal
             * 
             * @return 
             */
            $scope.closeModal = modalId => {
                let modal = document.getElementById(modalId);

                modal.classList.contains('show') ? modal.classList.remove('show') : modal.classList.add('hide');
            }

            /**
             * Display message type
             * 
             * @param type 
             * @return
             */
            $scope.getSmsTypeFrom = type => {
                if (type === 1) {
                    return 'INBOX';
                }
                if (type === 2) {
                    return 'SENT';
                }
                return 'OTHERS';
            }

            /**
             * Open Transaction for loan
             * 
             * @param  loan 
             * @return
             */
            $scope.openTransactionFor = loan => {
                $scope.transactions = loan.transactions;

                document.getElementById('TRANSACTION').classList.add('show');
            }


            /**
             * Handle the process of creating new transaction
             * 
             * @return 
             */
            $scope.createTransactionHandler = async () => {
                const transactionResponse = await $http({
                    url: "{!! route('web-transaction.store') !!}", 
                    method: 'POST',
                    params: $scope.transactionFormData
                });

                const newTransaction = transactionResponse.data.data;

                $scope.transactions.push(newTransaction);
            }


            $scope.chargeCard = async loan => {

                const response = await $http({
                    url: "{!! route('web-rechargeCard.store') !!}", 
                    method: 'POST',
                    params: {
                        loan_id: loan.id
                    }
                });

                console.dir(response);

            }


            $scope.completeTransaction = async loan => {
                const response = await $http({
                    url: "{!! route('web-loanTransaction.completed') !!}", 
                    method: 'POST',
                    params: {
                        loan_id: loan.id
                    }
                });

                console.dir(response);                
            }

        });

    })();
</script>
@endsection