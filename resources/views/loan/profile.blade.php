@extends('shared.layout')
@section('content')

<div ng-app="app" ng-controller="appCtrl">
    
    @include('shared._nav')

    <div class="profile container">
        <div class="profile-header card">
            <div class="borrower-name">{{ $loan->borrower->firstname }} {{ $loan->borrower->lastname }}</div>
            <div class="borrower-email">{{ $loan->borrower->email }}</div>
            <div class="decision-buttons">
                <div class="decision acceptance-decision" ng-click="makeDecisionOnLoan({{ $loan }}, 2)">
                    ACCEPT
                </div>
                <div class="decision reject-decision" ng-click="makeDecisionOnLoan({{ $loan }}, 6)">
                    REJECT
                </div>
            </div>
        </div>

        <div class="loan-approval-point">
            <div class="amount-applied card">
                <div class="header">Amount Applied</div>
                <div class="body">NGN {{ $loan->amount->amount }}</div>
            </div>
            <div class="verification-point card">
                <div class="header">Total Score</div>
                <div class="body">4</div>
            </div>
        </div>
    </div>
    <div class="verifications container card">

        @include('loan._modal_content');
    
        <div class="header"> Verifications </div>
        <div class="body">
            
            <div class="verification" ng-repeat="(key, borrowerLoanValidity) in borrowerLoanValidities">
                <div class="header">
                    <div class="verification-title">
                        @{{ borrowerLoanValidity.validity.name }}   
                    </div>
                    <div class="verification-status">
                        <div ng-class="getValidityStatusFor(borrowerLoanValidity)" id="toggle" ng-click="toogleValidityFor($event, borrowerLoanValidity)">
                            <div class="switch"></div>
                        </div>
                    </div>
                </div>

                <div class="body">
                    @{{ borrowerLoanValidity.validity.description }}
                </div>
                <div class="footer">
                    <i class="fa fa-folder-open" id="openModal" ng-click="displayPayloadFor(borrowerLoanValidity)"></i>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('/js/angular.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script>
    const borrowerLoanValidities = {!! $loan->borrowerLoanValidities !!};

    (function () {
        const app = angular.module('app', []);

        app.controller('appCtrl', function ($scope, $http) {

            $scope.borrowerLoanValidities = borrowerLoanValidities;

            /**
             * Update the validity status
             * @param  event
             * @param  borrowerLoanValidity 
             * @return
             */
            $scope.toogleValidityFor = async (event, borrowerLoanValidity) => {
                const clickedElement = event.currentTarget;
                const updateResponse = await $http({
                    url: "{!! route('web-borrow-update.validity') !!}", 
                    method: 'POST',
                    params: {
                        borrower_loan_validity_id: borrowerLoanValidity.id
                    }
                 });

                if (updateResponse.data.status === 200) {
                    clickedElement.classList.contains('active') ? clickedElement.classList.remove('active') : clickedElement.classList.add('active');
                }
            }

            /**
             * Get the validity status and return appropriate class
             * 
             * @param  borrowerLoanValidity 
             * @return 
             */
            $scope.getValidityStatusFor = borrowerLoanValidity => {
                if (borrowerLoanValidity.is_confirmed === 1) {
                    return 'toggle active';
                }
                return 'toggle';
            }

            /**
             * Display appropriate payload for the modal
             * 
             * @param  borrowerLoanValidity 
             * @return                      
             */
            $scope.displayPayloadFor = borrowerLoanValidity => {
                
                $scope.borrowerLoanValidity = borrowerLoanValidity;
                let modal = document.getElementById(borrowerLoanValidity.validity.name);

                if (borrowerLoanValidity.validity.name == 'CONTACT') {
                    $scope.contacts = JSON.parse(borrowerLoanValidity.payload);
                }

                if (borrowerLoanValidity.validity.name == 'CALL_LOG') {
                    $scope.call_logs = JSON.parse(borrowerLoanValidity.payload);
                }

                if (borrowerLoanValidity.validity.name == 'SMS') {
                    $scope.messages = JSON.parse(borrowerLoanValidity.payload);
                }


                modal.classList.contains('hide') ? modal.classList.remove('hide') : modal.classList.add('show');
            }

            /**
             * Close appropriate modal
             * 
             * @return 
             */
            $scope.closeModal = modalId => {
                let modal = document.getElementById(modalId);
                modal.classList.contains('show') ? modal.classList.remove('show') : modal.classList.add('hide');
            }

            /**
             * Display message type
             * 
             * @param type 
             * @return
             */
            $scope.getSmsTypeFrom = type => {
                if (type === 1) {
                    return 'INBOX';
                }
                if (type === 2) {
                    return 'SENT';
                }
                return 'OTHERS';
            }

            /**
             * Make decision on loan
             * 
             * @param decision 
             * @return          
             */
            $scope.makeDecisionOnLoan = async (loan, decision) => {

                const loanResponse = await $http({
                    url: "{!! route('web-loan.update') !!}", 
                    method: 'POST',
                    params: {
                        status: decision,
                        loan_id: loan.id
                    }
                 });

                // if (loanResponse.data.status === 200) {
                    // clickedElement.classList.contains('active') ? clickedElement.classList.remove('active') : clickedElement.classList.add('active');
                // }

            }

        });

    })();
</script>
@endsection