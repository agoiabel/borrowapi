@extends('shared.layout')
@section('content')
    <div class="homepage container">
        <div class="header">
            <div>
                <div class="page-title">BORROW</div>
                <div class="page-sub-title">EASIEST WAY TO GET LOAN</div>
            </div>
        </div>

        <div class="body">
            <p>
                Borrow provides you with loan options that meet your financial needs at anytime, loans up to 
                &#8358;45, 000 with interest rates as low as 1% per day.
            </p>

            <a href="{{ asset('app-release.apk') }}" class="download-link">
                <img src="{{ asset('/images/googlePlay.png') }}" alt="">
            </a>

            <p class="tutorial-link">
                Visit this <a href="" class="link">link</a> to watch a <a class="link">video tutorial</a> on how to use the application
            </p>

            <p class="">
                Having issues or want to say something, click on <a href="" class="link">here</a>   
            </p>
        </div>
    </div>
@endsection