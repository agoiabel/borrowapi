<?php

namespace App;

use Carbon\Carbon;
use App\Classes\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
	use Sluggable;

	/**
	 * Cast dates
	 * 
	 * @var 
	 */
	protected $dates = ['applied_date', 'date_to_refund', 'approved_date', 'date_refunded'];

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'slug', 'borrower_id', 'amount_id', 'applied_date', 'date_to_refund', 'approved_date', 'date_refund'
    ];

    /**
     * Generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
    	return $this->borrower->firstname .' '. $this->borrower->lastname; 	
    }

    /**
     * a loan belongs to an amount
     * 
     * @return 
     */
	public function amount()
	{
		return $this->belongsTo(Amount::class, 'amount_id');
	}

	/**
	 * a loan belongs to a borrower
	 * 
	 * @return 
	 */
	public function borrower()
	{
		return $this->belongsTo(User::class, 'borrower_id');
	}

	/**
	 * a loan has one borrowerLoanValidity
	 * 
	 * @return 
	 */
	public function borrowerLoanValidities()
	{
		return $this->hasMany(BorrowerLoanValidity::class, 'loan_id');
	}

	/**
	 * Handle the process of mutating date to refund
	 * 
	 * @param 
	 */
	public function setDateToRefundAttribute($date_to_refund)
	{
		return $this->attributes['date_to_refund'] = Carbon::parse($date_to_refund);
	}

	/**
	 * Handle the process of mutating date to refund
	 * 
	 * @param 
	 */
	public function setApprovedDateAttribute($approved_date)
	{
		return $this->attributes['approved_date'] = Carbon::parse($approved_date);
	}

	/**
	 * A loan has one active borrower
	 * 
	 * @return 
	 */
	public function activeBorrower()
	{
		return $this->hasOne(Loan::class, 'loan_id');
	}

	/**
	 * A loan has many transactions
	 * 
	 * @return 
	 */
	public function transactions()
	{
		return $this->hasMany(Transaction::class, 'loan_id');
	}
}