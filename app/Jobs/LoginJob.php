<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($request)
    {
        try {
            $user = $this->user->where('email', $request->email)->firstOrFail();    

            //compare password
            if (! $this->samePasswordFor($request->password, $user) ) {
                throw new \App\Exceptions\Custom('/', "bad credentials, seems something is wrong with password");    
            }

            //update user
            $user->update([
                'auth_token' => \App\Classes\Generate::token()
            ]);

            //authenticate user
            Auth::login($user, true);

            return $user;
        } catch (ModelNotFoundException $e) {
            return $this->user->create([
                'email' => $request->email,
                'password' => $request->password,
                'role_id' => Role::BORROWER,
                'auth_token' => \App\Classes\Generate::token(),
                'status' => User::DEFAULT
            ]);
        }
    }

    /**
     * Compare the password
     * 
     * @param  inputted $password 
     * @param  $user     
     * @return 
     */
    private function samePasswordFor($password, $user)
    {
        return Hash::check($password, $user->password); 
    }

}