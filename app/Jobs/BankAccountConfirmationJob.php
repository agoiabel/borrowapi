<?php

namespace App\Jobs;

use App\Classes\Paystack;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class BankAccountConfirmationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $paystack;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Paystack $paystack)
    {
        $this->paystack = $paystack;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($request)
    {
        // return $request->bvn;

        return $this->paystack->makeRequest('GET', 'https://api.paystack.co/bank/resolve_bvn/='.(int)$request->bvn);
        // return $this->paystack->makeRequest('GET', 'https://api.paystack.co/bank/match_bvn?account_number='.$request->account_number.'&bank_code='.$request->bank_name.'&bvn='.$request->bvn);



    }
}