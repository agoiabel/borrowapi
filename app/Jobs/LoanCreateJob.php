<?php

namespace App\Jobs;

use App\User;
use App\Loan;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Events\LoanWasCreated;
use App\Http\Requests\LoanFormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoanCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user, $loan;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Loan $loan)
    {
        $this->user = $user;

        $this->loan = $loan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(LoanFormRequest $request)
    {
        try {
            $borrower = $this->user->where('auth_token', $request->auth_token)->where('status', User::DEFAULT)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new \App\Exceptions\Custom('/', "user are not eligible to collect loan");
        }

       $loan = $this->loan->create([
            'borrower_id' => $borrower->id,
            'amount_id' => $request->loan_amount,
            'applied_date' => Carbon::now(),
            'date_to_refund' => $request->refund_date
        ]);

        $borrower->update([
            'status' => User::AWAITING_LOAN,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'gender' => $request->gender,
            'loan_id' => $loan->id
        ]);

        event(new LoanWasCreated($request, $loan));
    }
}