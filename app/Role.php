<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
    const BORROWER = 3;
    

	/**
	 * Attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name'
    ];

    /**
     * User can be mass assigned
     * 
     * @return 
     */
    public function user()
    {
    	return $this->hasMany(User::class, 'role_id');
    }
}