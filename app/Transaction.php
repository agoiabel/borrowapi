<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

	const ACCESS_PAYMENT_CODE = 'ACCESS_PAYMENT_CODE';
	const PAY_LOAN = 'PAY_LOAN';
    const LOAN_REBURSEMENT = 'LOAN_REBURSEMENT';
	const LOAN_REBURSEMENT_PARTIAL = 'LOAN_REBURSEMENT_PARTIAL';

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'user_id', 'loan_id', 'type', 'amount', 'confirmed', 'reference'
    ];

    /**
     * a transaction belongs to a user
     * 
     * @return 
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * a transaction belongs to a loan
     * 
     * @return 
     */
    public function loan()
    {
    	return $this->belongsTo(Loan::class, 'loan_id');
    }
}