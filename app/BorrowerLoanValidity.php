<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerLoanValidity extends Model
{
	/**
	 * Attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'borrower_id', 'validity_id', 'loan_id', 'is_confirmed', 'payload'
    ];

    /**
     * a borrowerLoanValidity belongs to a user
     * 
     * @return 
     */
    public function borrower()
    {
    	return $this->belongsTo(User::class, 'borrow_id');
    }

    /**
     * a borrowerLoanValidity belongs to a validity
     * 
     * @return 
     */
    public function validity()
    {
    	return $this->belongsTo(Validity::class, 'validity_id');
    }

    /**
     * a borrowerLoanValidity belongs to a loan
     * 
     * @return 
     */
    public function loan()
    {
    	return $this->belongsTo(Loan::class, 'loan_id');
    }
}