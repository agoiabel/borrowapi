<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const DEFAULT = 0;                              // started application but we dont know about it yet
    const AWAITING_LOAN = 1;                        // applied but we have not made a decision yet
    const LOAN_APPROVED = 2;                        // applied, we have approved the loan. but yet to confirm is registration payment
    const LOAN_CONFIRMATION_COMPLETED = 3;          // he has paid his loan confirmation and awaiting our money in his account
    const LOAN_GIVEN = 4;                           // we have sent money to his account
    const LOAN_PAID_BACK = 5;                       // he has paid back our loan
    const LOAN_DECLINED = 6;                        // he was declined loan collection

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'firstname', 'lastname', 'gender', 'status', 'role_id', 'auth_token', 'paystack_authorization_code', 'loan_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Mutate password
     * 
     * @param 
     */
    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }

    /**
     * A user has many loans
     * 
     * @return 
     */
    public function loans()
    {
        return $this->hasMany(Loan::class, 'borrower_id');
    }
    
    /**
     * a user belongs to a loan
     * 
     * @return 
     */
    public function activeLoan()
    {
        return $this->belongsTo(Loan::class, 'loan_id');   
    }

}