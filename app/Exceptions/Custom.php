<?php 

namespace App\Exceptions;

class Custom extends \Exception
{
	public function __construct($url, $error_message)
	{
		$this->url = $url;

		$this->error_message = $error_message;
	}
}