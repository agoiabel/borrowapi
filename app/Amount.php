<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amount extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'amount', 'description', 'percentage'
    ];

    /**
     * an amount has many loans
     * 
     * @return 
     */
    public function loans()
    {
    	return $this->hasMany(Loan::class, 'amount_id');
    }
}