<?php

namespace App\Mail;

use App\User;
use App\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Classes\Loan as LoanHelper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoanAcceptedFor extends Mailable
{
    use Queueable, SerializesModels;

    public $loan, $borrower, $loanHelper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $borrower, Loan $loan)
    {
        $this->loan = $loan;

        $this->borrower = $borrower;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(loanHelper $loanHelper)
    {
        return $this->view('emails.loan_accepted')
                    ->with([
                        'refundAmount' => $loanHelper->total_refund($this->loan)
                    ]);
    }
}