<?php

namespace App\Classes\Validity;

class Guarantor extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->guarantor_name.'-'.$request->guarantor_relationship.'-'.$request->guarantor_phone_number);
	}
}