<?php

namespace App\Classes\Validity;

class Identity extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		$file = base64_decode($request->image_path);
        $folderName = 'uploads/';

        $safeName = time().str_random(10).'.'.'png';
        file_put_contents(public_path().'/uploads/'.$safeName, $file);

        $image_path = $folderName.$safeName;

		return $this->store($borrower_id, $validity_id, $loan_id, $image_path);
	}
}