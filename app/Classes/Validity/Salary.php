<?php

namespace App\Classes\Validity;

class Salary extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->monthly_salary);
	}
}