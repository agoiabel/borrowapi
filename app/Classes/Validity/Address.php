<?php

namespace App\Classes\Validity;

use App\State;

class Address extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->address.'-'.$request->city.'-'.State::where('id', $request->state)->first()->name);
	}
}