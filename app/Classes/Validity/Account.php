<?php

namespace App\Classes\Validity;

class Account extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->bank_name.'-'.$request->account_number);
	}
}