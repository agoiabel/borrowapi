<?php

namespace App\Classes\Validity;

class Employer extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->company_name.'-'.$request->employer_name.'-'.$request->employer_phone_number.'-'.$request->job_title);
	}
}