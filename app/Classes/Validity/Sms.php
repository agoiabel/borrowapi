<?php

namespace App\Classes\Validity;

class Sms extends Store 
{
	public function handle($borrower_id, $validity_id, $loan_id, $request)
	{
		return $this->store($borrower_id, $validity_id, $loan_id, $request->sms);
	}
}