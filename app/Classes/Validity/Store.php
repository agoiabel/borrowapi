<?php

namespace App\Classes\Validity;

use App\BorrowerLoanValidity;

class Store {
    
    /**
     * Handle the process of creating new borrowerLoanValidity
     * 
     * @param  $borrow_id, $validity_id, $loan_id, $payload, $status   
     * @return              
     */
    public function store($borrower_id, $validity_id, $loan_id, $payload, $is_confirmed = FALSE)
    {
        return BorrowerLoanValidity::create([
            'borrower_id' => $borrower_id,
            'validity_id' => $validity_id,
            'loan_id' => $loan_id,
            'payload' => $payload,
            'is_confirmed' => $is_confirmed
        ]);        
    }

}