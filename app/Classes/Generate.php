<?php

namespace App\Classes;

class Generate 
{
	/**
	 * Generate token
	 * 
	 * @return 
	 */
	public static function token()
	{
		return uniqid() . time();
	}
}