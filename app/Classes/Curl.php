<?php

namespace App\Classes;

class Paystack 
{
	public function makeRequest($method, $endPoint, $formData = null)
	{
		$result = array();
		$ch = curl_init();
		$url = $endPoint;
		// $postdata = array('email' => 'customer@email.com', 'amount' => 500000,"reference" => '7PVGX8MEk85tgeEpVDtD');

		switch ($method){

		  case "POST":
		     curl_setopt($ch, CURLOPT_POST, 1);
		     if ($formData) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($formData));		     	
		     }
		     break;

		  case "PUT":
		     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		     if ($formData) {
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $formData);		     	
		     }
		     break;

		  default:
		     if ($formData) {
		        $url = sprintf("%s?%s", $url, http_build_query($formData));		     	
		     }
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = [
		  'Authorization: Bearer sk_live_2ef996b9a5559c95d627daf462f6e76da675e105',
		  // 'Authorization: Bearer sk_test_da5d641bf006413a188c2b383c4b263823e195a5',
		  'Content-Type: application/json',
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$request = curl_exec($ch);
		curl_close($ch);

		if ($request) {
		  return json_decode($request, true);
		}
	}

}