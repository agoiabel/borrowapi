<?php

namespace App\Classes;

use App\User;
use Carbon\Carbon;
use App\Transaction;
use App\Classes\Paystack;
use App\Classes\Loan as LoanHelper;

class ChargeCard 
{
	public $paystack, $loanHelper;


	public function __construct(LoanHelper $loanHelper, Paystack $paystack)
	{
		$this->paystack = $paystack;

		$this->loanHelper = $loanHelper;		
	}


	public function for($loan)
	{
        $transaction = Transaction::create([
            'type' => Transaction::LOAN_REBURSEMENT,
            'user_id' => $loan->borrower->id,
            'loan_id' => $loan->id,
            'amount' => $this->loanHelper->today_refund($loan),
            'reference' => uniqid().time(),
            'confirmed' => false
        ]);

        $chargeResult = $this->paystack->chargeReturningCustomer($loan, $transaction);

        if ($chargeResult['status'] == 200) {

            $paymentResult = $this->paystack->verifyPaymentFor($chargeResult['reference']);

            if ($paymentResult['status'] == 200) {
                $transaction->update([
                  'confirmed' => TRUE
                ]);
                $loan->update([
                  'date_refund' => Carbon::now()
                ]);
                $loan->borrower->update([
                  'status' => User::LOAN_PAID_BACK
                ]);
                return response()->json([
                    'message' => 'You have successfully paid your loan',
                    'status' => 200
                ], 200);
            }

        }

        return response()->json([
            'message' => 'We were unable to charge your card',
            'status' => 422
        ], 200);
	}
}