<?php

namespace App\Classes;

class Paystack 
{
    /**
     * Handle the process of loan verification
     * 
     * @param  $reference 
     * @return            
     */
    public function verifyPaymentFor($reference)
    {
        $status = [];
        $result = [];
        //The parameter after verify/ is the transaction reference to be verified
        $url = 'https://api.paystack.co/transaction/verify/'.$reference;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
          $ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer sk_test_da5d641bf006413a188c2b383c4b263823e195a5'
          ]
        );
        $request = curl_exec($ch);
        curl_close($ch);

        if ($request) {

            $result = json_decode($request, true);
            if ($result) {
              if ($result['data']) {
                //something came in
                if ($result['data']['status'] == 'success') {
                    $status['status'] = 200;
                    $status['reference'] = $reference;
                    $status['message'] = $result['data']['authorization']['authorization_code'];
                } else {
                    $status['status'] = 422;
                    $status['reference'] = $reference;
                    $status['message'] = $result['data']['gateway_response'];
                }
              } else {
                    $status['status'] = 422;
                    $status['reference'] = $reference;
                    $status['message'] = $result['message'];
              }

            } else {
              print_r($result);
            }
        } else {
            var_dump($request);
        }

        return $status;
    }


    /**
     * Confirm Charging customer
     * 
     * @param  $loan, $amount, $reference 
     * @return            
     */
    public function chargeReturningCustomer($loan, $transaction)
    {
        $result = [];
        $status = [];

        $postdata =  [
            'authorization_code' => $loan->borrower->paystack_authorization_code,
            'email' => $loan->borrower->email, 
            'amount' => $transaction->amount * 100,
            "reference" => $transaction->reference
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.paystack.co/transaction/charge_authorization");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
          'Authorization: Bearer sk_test_da5d641bf006413a188c2b383c4b263823e195a5',
          'Content-Type: application/json',
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $request = curl_exec($ch);
        curl_close ($ch);

        if ($request) {
          $result = json_decode($request, true);

          if ($result) {

             if ($result['data']) {
                if ($result['data']['status'] == 'success') {
                    $status['status'] = 200;
                    $status['reference'] = $result['data']['reference'];
                }
             }
          }
        }

        return $status;
    }


}