<?php 

namespace App\Classes;

use Carbon\Carbon;

/**
 * 
 */
class Loan
{	
	public function __construct()
	{
		# code...
	}

	/**
	 * Calculate the interest per day
	 * 
	 * @param  $loan 
	 * @return       
	 */
	public function interest_per_day($loan)
	{
		return ($loan->amount->percentage / 100) * (1 / 30);
	}

	/**
	 * Get the total interest
	 * 
	 * @return 
	 */
	public function total_interest($interest_per_day, $loan_days)
	{
		return $interest_per_day * $loan_days;
	}

	/**
	 * Total refund on a loan
	 * 
	 * @param  $date_approved, $date_to_refund 
	 * @return                 
	 */
	public function total_refund($loan)
	{
		$interest_per_day = $this->interest_per_day($loan);

		$total_interest = $this->total_interest($interest_per_day, $loan->date_to_refund->diffInDays($loan->approved_date));

		return ($total_interest * $loan->amount->amount) + $loan->amount->amount;
	}

	/**
	 * Amount to refund today
	 * 
	 * @param  $loan 
	 * @return       
	 */
	public function today_refund($loan)
	{
		$number_of_days_to_hold_loan = $loan->date_to_refund->diffInDays($loan->approved_date); 
		$number_of_days_loan_used = $loan->approved_date->diffInDays(Carbon::now());
		$maximum_days_loan_interest_can_get_to = $number_of_days_to_hold_loan + 7;  
  
		$interval = 0;

		if ($number_of_days_loan_used > $maximum_days_loan_interest_can_get_to) {
			$interval = maximum_days_loan_interest_can_get_to;
		}
		$interval = $loan->approved_date->diffInDays(Carbon::now()) + 1;

		$total_interest = $this->total_interest($this->interest_per_day($loan), $interval);

		return ($total_interest * $loan->amount->amount) + $loan->amount->amount;
	}


	
}