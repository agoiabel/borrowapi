<?php

namespace App\Listeners;

use App\User;
use App\Loan;

use App\Mail\LoanAcceptedFor;
use App\Mail\LoanRejectedFor;

use App\Events\LoanStatusUpdated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailBorrower
{
    public $borrower;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $borrower)
    {
        $this->borrower = $borrower;
    }

    /**
     * Handle the event.
     *
     * @param  LoanStatusUpdated  $event
     * @return void
     */
    public function handle(LoanStatusUpdated $event)
    {
        $borrower = $this->borrower->where('id', $event->loan->borrower_id)->firstOrFail();

        if ($borrower->status == User::LOAN_APPROVED) {
            return Mail::to($borrower)->send(new LoanAcceptedFor($borrower, $event->loan));
        }

        return Mail::to($borrower)->send(new LoanRejectedFor($borrower, $event->loan)); 
    }
}