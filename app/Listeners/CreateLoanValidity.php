<?php

namespace App\Listeners;

use App\Validity;

use App\Events\LoanWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateLoanValidity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Validity $validity)
    {
        $this->validity = $validity;
    }


    /**
     * password lookup
     * 
     * @var 
     */
    public static $lookup = [
        'CONTACT' => \App\Classes\Validity\Contact::class,
        'CALL_LOG' => \App\Classes\Validity\CallLog::class,
        'PHONE_NUMBER' => \App\Classes\Validity\Phonenumber::class,
        'BVN' => \App\Classes\Validity\Bvn::class,
        'ACCOUNT_DETAILS' => \App\Classes\Validity\Account::class,
        'GUARANTOR' => \App\Classes\Validity\Guarantor::class,
        'EMPLOYER' => \App\Classes\Validity\Employer::class,
        'IDENTITY' => \App\Classes\Validity\Identity::class,
        'SALARY_RANGE' => \App\Classes\Validity\Salary::class,
        'ADDRESS' => \App\Classes\Validity\Address::class,
        'SMS' => \App\Classes\Validity\Sms::class,
    ];


    /**
     * Handle lookup and redirect
     *       
     * @return                 
     */
    public function store($event, $validity)
    {
        (new static::$lookup[$validity->name])->handle($event->loan->borrower_id, $validity->id, $event->loan->id, $event->request);
    }

    /**
     * Handle the event.
     *
     * @param  LoanWasCreated  $event
     * @return void
     */
    public function handle(LoanWasCreated $event)
    {
        foreach ($this->validity->get() as $validity) {            
            $this->store($event, $validity);
        }
    }
}