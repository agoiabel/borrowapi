<?php

namespace App\Listeners;

use App\Loan;
use App\User;
use App\Events\LoanStatusUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanStatusUpdated  $event
     * @return void
     */
    public function handle(LoanStatusUpdated $event)
    {
        if ($event->loan->status == Loan::APPROVED) {
            $event->loan->borrower->update([
                'status' => User::LOAN_APPROVED
            ]);
        }
        if ($event->loan->status == Loan::DECLINED) {
            $event->loan->borrower->update([
                'status' => User::DEFAULT
            ]);
        }
    }
}
