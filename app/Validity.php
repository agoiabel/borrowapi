<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validity extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name', 'description'
    ];

    
}
