<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name'
    ];
}