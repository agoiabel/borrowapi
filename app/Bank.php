<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name', 'code'
    ];
}