<?php

namespace App\Http\Middleware;

use App\User;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectIfApiAuthenticated
{
    public $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;    
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( empty($request->header('AuthToken')) ) {
            return response()->json([
                'message' => 'AuthToken not present in request'
            ], 401);
        }

        try {
            $this->auth->login( User::where('auth_token', $request->header('AuthToken'))->firstOrFail() );
            return $next($request);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'bad request, seems your token expired'
            ], 400);   
        }
    }
}
