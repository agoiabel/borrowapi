<?php

namespace App\Http\Requests;

use App\Loan;
use Illuminate\Foundation\Http\FormRequest;

class LoanRebursementFormRequest extends FormRequest
{
    public $loan;

    public function __construct(Loan $loan)
    {
        $this->loan = $loan;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Handle the process of loan rebursement
     * 
     * @return 
     */
    public function handle()
    {
        $loan = $this->loan->with(['borrower'])->where('id', $this->loan_id)->firstOrFail();

        $authorization_code = $loan->borrower->paystack_authorization_code;
        $email = $loan->borrower->email;
        $amount = $this->loanHelper->total_refund($loan) * 100;


    }
}