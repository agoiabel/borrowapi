<?php

namespace App\Http\Requests;

use App\Jobs\BankAccountConfirmationJob;
use Illuminate\Foundation\Http\FormRequest;

class BankConfirmationFormRequest extends FormRequest
{
    public $job;

    public function __construct(BankAccountConfirmationJob $job)
    {
        $this->job = $job;        
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Handle the process of confirming user account
     * 
     * @return 
     */
    public function handle()
    {
        return $this->job->handle($this);
    }
}