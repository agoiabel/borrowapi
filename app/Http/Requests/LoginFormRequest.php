<?php

namespace App\Http\Requests;

use App\Jobs\LoginJob;
use Illuminate\Foundation\Http\FormRequest;

class LoginFormRequest extends FormRequest
{
    public $job;

    public function __construct(LoginJob $job)
    {
        $this->job = $job;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    /**
     * Handle the process of redirecting to LoginFormRequest
     * 
     * @return 
     */
    public function handle()
    {
        return $this->job->handle($this);
    }

}