<?php

namespace App\Http\Requests;

use App\Loan;
use App\Transaction;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Http\FormRequest;

class InitiateTransactionFormRequest extends FormRequest
{
    public $loan, $auth, $transaction;

    public function __construct(Loan $loan, Guard $auth,Transaction $transaction)
    {
        $this->loan = $loan;

        $this->auth = $auth;

        $this->transaction = $transaction;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Handle initiaiting transaction
     * 
     * @return 
     */
    public function handle()
    {
        // $loan = $this->loan->where('id', $this->loan_id)->firstOrFail();

        // $transaction = $this->transaction->create([
        //     'type' => $this->type,
        //     'user_id' => $this->auth->user()->id,
        //     'loan_id' => $loan->id,
        //     'amount' => ($this->type == Loan::ACCESS_PAYMENT_CODE) ? '100' : $loan->amount->amount,
        //     'reference' => uniqid().time()
        // ]);

        // return $transaction;
    }
}