<?php

namespace App\Http\Requests;

use App\Jobs\LoanCreateJob;
use Illuminate\Foundation\Http\FormRequest;

class LoanFormRequest extends FormRequest
{
    public $job;

    public function __construct(LoanCreateJob $job)
    {
        $this->job = $job;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Handle my job
     * 
     * @return 
     */
    public function handle()
    {
        return $this->job->handle($this);
    }
}