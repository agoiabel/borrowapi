<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateValidityFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'borrower_loan_validity_id' => 'required'
        ];
    }

    /**
     * Handle the process of updating loan validity
     * 
     * @return 
     */
    public function handle()
    {
        $borrowerLoanValidity = \App\BorrowerLoanValidity::where('id', $this->borrower_loan_validity_id)->firstOrFail();

        $borrowerLoanValidity->update([
            'is_confirmed' => ! $borrowerLoanValidity->is_confirmed
        ]);
    }
}
