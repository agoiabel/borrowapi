<?php

namespace App\Http\Requests;

use App\Loan;
use App\User;
use Carbon\Carbon;
use App\Events\LoanStatusUpdated;
use Illuminate\Foundation\Http\FormRequest;

class LoanStatusFormRequest extends FormRequest
{
    public $loan;

    public function __construct(Loan $loan)
    {
        $this->loan = $loan;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Handle the process of updating loan
     * 
     * @return 
     */
    public function handle()
    {
        $loan = $this->loan->with(['borrower'])->where('id', $this->loan_id)->firstOrFail();

        $loan->borrower->update([
            'status' => $this->status
        ]);

        //update loan status based on parameter sent
        $loan->update([
            'approved_date' => Carbon::now()
        ]);

        event(new LoanStatusUpdated($loan));
    }
}