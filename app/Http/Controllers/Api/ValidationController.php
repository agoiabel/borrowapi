<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankConfirmationFormRequest;

class ValidationController extends Controller
{
	/**
	 * Handle the process of confirming bank
	 * 
	 * @param  Request $request 
	 * @return  
	 */
    public function confirmBank(BankConfirmationFormRequest $request)
    {
    	return $request->handle();

    		
    }
}