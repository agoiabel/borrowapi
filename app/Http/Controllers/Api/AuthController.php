<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Bank;
use App\Loan;
use App\State;
use App\Amount;
use Illuminate\Http\Request;
use App\Classes\Loan as LoanHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthController extends Controller
{
    public $amount, $bank, $loan, $loanHelper;

    public function __construct(Bank $bank, Loan $loan, Amount $amount, LoanHelper $loanHelper)
    {
        $this->bank = $bank;

        $this->loan = $loan;

        $this->amount = $amount;

        $this->loanHelper = $loanHelper;
    }

	/**
	 * Handle the process of storing authenticating user
	 * 
	 * @return 
	 */
    public function store(LoginFormRequest $request)
    {
        try {
        	return $this->authenticateUserFrom($request);
        } catch (\App\Exceptions\Custom $e) {
            return response()->json([
            	'error' => 'Your password is wrong, click the reset link or try again['.time() .']', 
            	'status' => 422
			], 422);
        }
    }

    /**
     * Handle the process of authenticating new user
     * 
     * @return 
     */
    public function authenticateUserFrom($request)
    {
        $user = $request->handle();

        $array = [];
        $array['user'] = $user;
        $array['states'] = State::all();
        $array['banks'] = Bank::all();
        $array['amounts'] = Amount::all();            
        $array['status'] = 200;       

        if ($user->status !== User::DEFAULT) {
            $loan = $this->loan->where('id', $user->loan_id)->firstOrFail();

            $array['loan_status'] = [
                'loan_id' => $loan->id,
                'loan_due_date' => $loan->date_to_refund->toDateTimeString(),
                'loan_total_due' =>  $this->loanHelper->total_refund($loan)
            ];                

            if ($user->status !== User::AWAITING_LOAN) {
                $array['loan_status'] = [
                    'loan_id' => $loan->id,
                    'loan_due_date' => $loan->date_to_refund->toDateTimeString(),
                    'loan_total_due' =>  $this->loanHelper->total_refund($loan),
                    'loan_current_due' => $this->loanHelper->today_refund($loan)
                ];
            }
            // $array['loan_id'] = $loan->id;
            // $array['loan_due_date'] = $loan->date_to_refund->toDateTimeString();
            // $array['loan_current_due'] = $this->loanHelper->today_refund($loan);
            // $array['loan_total_due'] =  $this->loanHelper->total_refund($loan);   
        }


        return response()->json($array, 200);
    }

}