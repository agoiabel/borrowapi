<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InitiateTransactionFormRequest;

class TransactionController extends Controller
{
	/**
	 * Handle the process initiate transaction
	 * 
	 * @return 
	 */
    public function initiate(InitiateTransactionFormRequest $request)
    {
    	return response()->json([
    		'transaction' => $request->handle()
    	]);	
    }
}