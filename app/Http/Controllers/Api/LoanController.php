<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Loan;
use App\Transaction;
use App\Classes\ChargeCard;

use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoanFormRequest;
use App\Http\Requests\LoanRebursementFormRequest;


class LoanController extends Controller
{
	/**
	 * Handle the process of storing loan
	 * 
	 * @param  LoanFormRequest $request 
	 * @return 
	 */
    public function store(LoanFormRequest $request)
    {
    	try {
  			$request->handle();    		

  			return response()->json([
  				'message' => 'Loan application succesful, we will get back intouch after verification',
  				'status' => 200
  			], 200);
    	} catch (\App\Exceptions\Custom $e) {
			return response()->json([
  				'message' => 'Your credentials are been verified, we will get back to you shortly via email and phone call',
  				'status' => 422
  			], 422);
    	}
    }

    /**
     * Handle rebursement
     * 
     * @return 
     */
    public function rebursement(Request $request, ChargeCard $chargeCard)
    {
        $loan = Loan::with(['borrower'])->where('id', $request->loan_id)->firstOrFail();

        if ($loan->borrower->status === User::LOAN_PAID_BACK) {
            return response()->json([
                'message' => 'You have successfully paid your loan',
                'status' => 200
            ], 200);
        }

        return $chargeCard->for($loan);
    }


    /**
     * Handle the process of ending loan
     * 
     * @param  Request $request 
     * @return 
     */
    public function end(Request $request, User $user)
    {
        $user->where('id', $request->user_id)->firstOrFail()->update([
          'status' => User::DEFAULT,
          'loan_id' => NULL
        ]);

        return response()->json([
          'message' => 'Loan ended successfully',
          'status' => 200
        ], 200);
    }

}