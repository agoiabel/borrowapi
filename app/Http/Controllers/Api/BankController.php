<?php

namespace App\Http\Controllers\Api;

use App\Classes\Paystack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
	/**
	 * Get all banks
	 * 
	 * @return 
	 */
    public function index(Paystack $paystack)
    {
    	return response()->json([
    		'bankRequest' => $paystack->makeRequest('GET', 'https://api.paystack.co/bank')
    	]);
    }
}