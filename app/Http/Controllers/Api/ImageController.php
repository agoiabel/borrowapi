<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
	/**
	 * Handle the process of storing image
	 * 
	 * @param  Request $request 
	 * @return 
	 */
    public function store(Request $request)
    {
		$file = base64_decode($request->image);
        $folderName = 'uploads/';

        $safeName = time().str_random(10).'.'.'png';
        file_put_contents(public_path().'/uploads/'.$safeName, $file);

        return response()->json([
        	'image_path' => $folderName.$safeName
        ], 200);
    }

}