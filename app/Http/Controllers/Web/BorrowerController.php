<?php

namespace App\Http\Controllers\Web;

use App\Loan;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateValidityFormRequest;

class BorrowerController extends Controller
{
	/**
	 * Display view to show awaiting borrowers
	 * 
	 * @return 
	 */
    public function awaiting(User $user)
    {
        // $loans = $loan->where('status', Loan::UNAPPROVED)->get();
        $users = $user->with(['activeLoan.borrower', 'activeLoan.amount'])->where('status', User::AWAITING_LOAN)->get();

    	return view('loan.awaiting-borrow', compact('users'));
    }

    /**
     * Display view to show loan profile
     * 
     * @return 
     */
    public function loan($loan_slug, Loan $loan)
    {
        $loan = $loan->with(['borrower', 'amount', 'borrowerLoanValidities.loan', 'borrowerLoanValidities.validity'])->where('slug', $loan_slug)->firstOrFail();

    	return view('loan.profile', compact('loan'));
    }

    /**
     * Handle the process of updating validity
     * 
     * @return 
     */
    public function updateValidity(UpdateValidityFormRequest $request)
    {
        $request->handle();

        return response()->json([
            'message' => 'status was updated successfully',
            'status' => 200
        ], 200);
    }
}