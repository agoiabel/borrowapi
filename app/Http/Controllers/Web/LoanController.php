<?php

namespace App\Http\Controllers\Web;

use App\Loan;
use App\User;
use Carbon\Carbon;
use App\Transaction;
use App\Classes\ChargeCard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\LoanStatusFormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Classes\Paystack;

class LoanController extends Controller
{
	/**
	 * Update loan status
	 * 
	 * @return 
	 */
    public function status(LoanStatusFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'status updated successfully',
    		'status' => 200
    	], 200);
    }

    /**
     * Charge card
     * 
     * @return 
     */
    public function chargeCard(Loan $loan, Guard $auth, $loan_slug)
    {
        $loan = $loan->with(['borrower'])->whereSlug($loan_slug)->firstOrFail();
        
        try {
            
            $transaction = Transaction::where('loan_id', $loan->id)->where('type', Transaction::ACCESS_PAYMENT_CODE)->firstOrFail();

        } catch (ModelNotFoundException $e) {

            $transaction = Transaction::create([
                'type' => Transaction::ACCESS_PAYMENT_CODE,
                'user_id' => $loan->borrower_id,
                'loan_id' => $loan->id,
                'amount' => '100',
                'reference' => uniqid().time()
            ]);

        }

        return view('loan.chargeCard', compact('loan', 'transaction'));
    }

    /**
     * Confirm payment
     * 
     * @param $reference 
     * @param  Loan   $loan      
     * @return            
     */
    public function confirmCard($transaction_reference, $paystack_reference, Paystack $paystack)
    {
        $status = $paystack->verifyPaymentFor($paystack_reference);

        if ($status['status'] == 200) {
            try {

                $transaction = Transaction::with(['user', 'loan'])->where('reference', $transaction_reference)->firstOrFail();

                $transaction->update([
                    'confirmed' => TRUE
                ]);
                $transaction->user->update([
                  'paystack_authorization_code' => $status['message'],
                  'status' => User::LOAN_CONFIRMATION_COMPLETED
                ]);

                session()->flash('message', 'Hold on, you will receive an alert real soon');
                return redirect()->back();

            } catch (ModelNotFoundException $e) {

                session()->flash('message', 'Opps, something went wrong');
                return redirect()->back();

            }
        }

        //email user that money wasnt received so cannot process loan 
        session()->flash('message', 'We could not process your card');

        return redirect()->back();
    }

    /**
     * Display page of loan users ready to payout
     * 
     * @return 
     */
    public function readyToPayout(User $user)
    {
        $users = $user->with(['activeLoan.amount', 'activeLoan.borrower'])->where('status', User::LOAN_CONFIRMATION_COMPLETED)->get();

        return view('loan.ready_to_payout', compact('users'));
    }

    /**
     * Handle the process of updating loan payout
     * 
     * @return 
     */
    public function payout(Loan $loan, Request $request)
    {
        $loan = $loan->with(['borrower'])->where('id', $request->loan_id)->firstOrFail();

        $loan->borrower->update([
          'status' => User::LOAN_GIVEN,
        ]);

        return response()->json([
          'message' => 'Loan status updated successfully',
          'status' => 200
        ]);
    }

    /**
     * display loan due three days from now
     * 
     * @return 
     */
    public function loanDueThreeDaysFromNow(Loan $loan)
    {
        $loans = $loan->whereBetween('date_to_refund', [Carbon::now()->toDateString(), Carbon::now()->addDays(3)->toDateString()])->whereNull('date_refund')->get();
        
        return view('loan.due_soon', compact('loans'));
    }

    /**
     * Display view to show profile
     * 
     * @return 
     */
    public function payBackProfile(Loan $loan, $loan_slug)
    {
        $loan = $loan->with(['borrower', 'amount', 'borrowerLoanValidities.loan', 'borrowerLoanValidities.validity', 'transactions'])->where('slug', $loan_slug)->firstOrFail();

        return view('loan.payBackProfile', compact('loan'));
    }

    /**
     * Handle the process of recharging card
     * 
     * @param  Request $request 
     * @return 
     */
    public function rechargeCard(Request $request, ChargeCard $chargeCard)
    {
        $loan = Loan::with(['borrower'])->where('id', $request->loan_id)->firstOrFail();

        if ($loan->borrower->status === User::LOAN_PAID_BACK) {
            return response()->json([
                'message' => 'You have successfully paid your loan',
                'status' => 200
            ], 200);
        }

        return $chargeCard->for($loan);
    }

    /**
     * Handle the process of ending a loan
     * 
     * @return 
     */
    public function loanTransactionCompleted(Request $request)
    {
        $loan = Loan::with(['borrower'])->where('id', $request->loan_id)->firstOrFail();

        $loan->update([
          'date_refund' => Carbon::now()
        ]);
        $loan->borrower->update([
          'status' => User::LOAN_PAID_BACK
        ]);

        return response()->json([
            'message' => 'You have successfully ended loan transaction',
            'status' => 200
        ], 200);
    }
}