<?php

namespace App\Http\Controllers\Web;

use App\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;

class AuthController extends Controller
{
	/**
	 * Display view to show login
	 * 
	 * @return 
	 */
    public function create()
    {
    	return view('auth.login');
    }

    /**
     * Handle the process of authenticating
     *  
     * @param  Request $request 
     * @return  
     */
    public function store(LoginFormRequest $request)
    {
    	if ($request->handle()->role_id === Role::ADMIN) {
    		return redirect()->route('web-borrow.awaiting');
    	}
    	return redirect()->back();
    }
}