<?php

namespace App\Http\Controllers\Web;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
	/**
	 * Handle the process of storing a new transaction
	 * 
	 * @return 
	 */
    public function store(Request $request, Transaction $transaction)
    {
    	$transaction = $transaction->create([
    		'user_id' => $request->user_id,
    		'loan_id' => $request->loan_id,
    		'type' => $request->type,
    		'amount' => $request->amount,
    		'confirmed' => TRUE
    	]);

		return response()->json([
			'data' => $transaction,
			'message' => 'transaction was created successfully'
		], 200);
    }
}