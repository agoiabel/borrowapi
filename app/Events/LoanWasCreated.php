<?php

namespace App\Events;

use App\Loan;
use App\Http\Requests\LoanFormRequest;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LoanWasCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $loan, $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(LoanFormRequest $request, Loan $loan)
    {
        $this->loan = $loan;

        $this->request = $request;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
