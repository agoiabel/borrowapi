<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->nullable();
            $table->integer('borrower_id')->unsigned();
            $table->integer('amount_id')->unsigned();

            $table->timestamp('applied_date')->nullable();
            $table->timestamp('approved_date')->nullable();
            $table->timestamp('date_to_refund')->nullable();
            $table->timestamp('date_refund')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
