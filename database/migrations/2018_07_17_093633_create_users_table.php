<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

            $table->integer('loan_id')->unsigned()->nullable();

            $table->string('email')->unique();
            $table->string('password');

            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('auth_token')->nullable();
            $table->string('paystack_authorization_code')->nullable();
            
            $table->string('gender')->nullable();
            $table->integer('status')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
