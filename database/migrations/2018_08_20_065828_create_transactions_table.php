<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->reference('id')->on('users')->onDelete('cascade');
            
            $table->integer('loan_id')->unsigned();
            // $table->foreign('loan_id')->reference('id')->on('loans')->onDelete('cascade');

            $table->string('type');
            $table->decimal('amount');
            $table->boolean('confirmed')->default(FALSE);
            $table->string('reference')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
