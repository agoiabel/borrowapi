<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowerLoanValiditiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrower_loan_validities', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('borrower_id')->unsigned();
            $table->foreign('borrower_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('validity_id')->unsigned();
            $table->foreign('validity_id')->references('id')->on('validities')->onDelete('cascade');

            $table->integer('loan_id')->unsigned();
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');

            $table->boolean('is_confirmed')->default(false);
            
            $table->longText('payload')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrower_loan_validities');
    }
}
