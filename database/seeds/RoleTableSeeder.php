<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	[
        		'name' => 'ADMIN'
        	],
        	[
        		'name' => 'INVESTOR'
        	],
        	[
        		'name' => 'BORROWER'
        	]
        ];

        foreach ($roles as $key => $role) {
        	Role::create($role);
        }
    }
}
