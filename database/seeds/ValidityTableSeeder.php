<?php

use App\Validity;
use Illuminate\Database\Seeder;

class ValidityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $validities = [
        	[
        		'name' => 'IDENTITY',
        		'description' => 'You have been able to confirm the borrower identity which includes name'
        	],
        	[
        		'name' => 'PHONE_NUMBER',
        		'description' => 'You have called and confirmed phone number'
        	],
        	[
        		'name' => 'BVN',
        		'description' => 'You have confirmed the BVN matches users account details'
        	],
        	[
        		'name' => 'SMS',
        		'description' => 'You have gotten access to the user,s SMS logs'
        	],
        	[
        		'name' => 'CALL_LOG',
        		'description' => 'You have gotten access to the user,s call logs'
        	],
        	[
        		'name' => 'CONTACT',
        		'description' => 'You have gotten access to the user,s phone contacts'
        	],
        	[
        		'name' => 'EMPLOYER',
        		'description' => 'You have confirmed the user.s employer number and place of work'
        	],
        	[
        		'name' => 'GUARANTOR',
        		'description' => 'You have called and confirmed the user guarantor'
        	],
            [
                'name' => 'ACCOUNT_DETAILS',
                'description' => 'You have called and confirmed the user guarantor'
            ],
            [
                'name' => 'SALARY_RANGE',
                'description' => 'You have called and confirmed the user guarantor'
            ],
            [
                'name' => 'ADDRESS',
                'description' => 'Confirm the borrower house address'
            ]
        ];

        foreach ($validities as $key => $validity) {
        	Validity::create($validity);
        }
    }
}