<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->createNewUser(Role::ADMIN, 'johndoe@admin.com', User::DEFAULT);

    	// $this->createNewUser(Role::BORROWER, 'johndoe1@borrower.com', User::DEFAULT);
    	// $this->createNewUser(Role::BORROWER, 'johndoe2@borrower.com', User::AWAITING_LOAN);
    	// $this->createNewUser(Role::BORROWER, 'johndoe3@borrower.com', User::LOAN_APPROVED);
    }

    /**
     * Create New Users
     * 
     * @param  $role_id, $email, $status  
     * @return  
     */
    public function createNewUser($role_id, $email, $status)
    {
		$faker = Faker\Factory::create();

		return User::create([
        	'role_id' => $role_id,
        	'email' => $email,
        	'password' => 'abc',
        	'firstname' => $faker->firstName,
        	'lastname' => $faker->lastName,
        	'gender' => 'MALE',
        	'status' => $status
        ]);
    }

}