<?php

use App\Bank;
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
		    [
		      'name' => 'Access Bank',
		      'code' => '044'
		    ],
		    [
		      'name' => 'Citibank Nigeria',
		      'code' => '023'
		    ],
		    [
		      'name' => 'Diamond Bank',
		      'code' => '063',
		    ],
		    [
		      'name' => 'Ecobank Nigeria',
		      'code' => '050',
		    ],
		    [
		      'name' => 'Enterprise Bank',
		      'code' => '084'
		    ],
		    [
		      'name' => 'Fidelity Bank',
		      'code' => '070',
		    ],
		    [
		      'name' => 'First Bank of Nigeria',
		      'code' => '011',
		    ],
		    [
		      'name' => 'First City Monument Bank',
		      'code' => '214',
		    ],
		    [
		      'name' => 'Guaranty Trust Bank',
		      'code' => '058',
		    ],
		    [
		      'name' => 'Heritage Bank',
		      'code' => '030',
		    ],
		    [
		      'name' => 'Keystone Bank',
		      'code' => '082',
		    ],
		    [
		      'name' => 'MainStreet Bank',
		      'code' => '014',
		    ],
		    [
		      'name' => 'Skye Bank',
		      'code' => '076',
		    ],
		    [
		      'name' => 'Stanbic IBTC Bank',
		      'code' => '221',
		    ],
		    [
		      'name' => 'Standard Chartered Bank',
		      'code' => '068',
		    ],
		    [
		      'name' => 'Sterling Bank',
		      'code' => '232',
		    ],
		    [
		      'name' => 'Union Bank of Nigeria',
		      'code' => '032',
		    ],
		    [
		      'name' => 'United Bank For Africa',
		      'code' => '033',
		    ],
		    [
		      'name' => 'Unity Bank',
		      'code' => '215',
		    ],
		    [
		      'name' => 'Wema Bank',
		      'code' => '035',
		    ],
		    [
		      'name' => 'Zenith Bank',
		      'code' => '057'
		    ]
        ];

        foreach ($banks as $key => $bank) {
        	Bank::create($bank);
        }
    }
}
