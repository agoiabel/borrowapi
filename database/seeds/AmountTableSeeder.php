<?php

use App\Amount;
use Illuminate\Database\Seeder;

class AmountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amounts = [
        	[
        		'amount' => '10000',
                'percentage' => '30',
        		'description' => 'step one person'
        	],
        	[
        		'amount' => '15000',
                'percentage' => '30',
        		'description' => 'step two person'
        	]
        ];

        foreach ($amounts as $key => $amount) {
        	Amount::create($amount);
        }
    }
}