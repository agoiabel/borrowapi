<?php

use App\User;
use App\Loan;
use App\Amount;
use App\Validity;
use Carbon\Carbon;
use App\BorrowerLoanValidity;
use Illuminate\Database\Seeder;

class LoanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        foreach (User::where('status', User::DEFAULT)->get() as $borrower) {
	        $loan = Loan::create([
	            'borrower_id' => $borrower->id,
	            'amount_id' => Amount::first()->id,
	            'status' => Loan::UNAPPROVED,
	            'applied_date' => Carbon::now(),
	            'date_to_refund' => Carbon::tomorrow(), 
	        ]);

	        $borrower->update([
	            'status' => User::AWAITING_LOAN,
	            'firstname' => $faker->firstname,
	            'lastname' => $faker->lastname,
	            'gender' => $faker->gender,
	        ]);

	        foreach (Validity::get() as $key => $validity) {
		        BorrowerLoanValidity::create([
		            'borrower_id' => $borrower->id,
		            'validity_id' => $validity->id,
		            'loan_id' => $loan->id,
		            'payload' => $payload,
		            'is_confirmed' => false
		        ]);    
	        }
        }



    }
}