<?php

Route::middleware(['cors'])->group(function () {


	Route::post('auth/store', 'Api\AuthController@store')->name('api-auth.store');
	// Route::post('bank/confirm', 'Api\ValidationController@confirmBank')->name('api-auth.confirmBank');
	
	Route::middleware(['Api-auth'])->group(function () {

		// Route::post('image/store', 'Api\ImageController@store')->name('api-image.store');
		
		Route::post('loan/store', 'Api\LoanController@store')->name('api-loan.store');
		Route::post('transaction/initiate', 'Api\TransactionController@initiate')->name('api-transaction.initiate');
		Route::post('loan/rebursement', 'Api\LoanController@rebursement')->name('api-loan.rebursement');
		Route::post('loan/end', 'Api\LoanController@end')->name('api-loan.end');

	});	

});