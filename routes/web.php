<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('auth/login', 'Web\AuthController@create')->name('web-auth.create');
Route::post('auth/login', 'Web\AuthController@store')->name('web-auth.store');

Route::get('loan/chargeCard/{loan_slug}', 'Web\LoanController@chargeCard')->name('web-loan.chargeCard');
Route::get('loan/confirmCard/{transaction_reference}/{paystack_reference}', 'Web\LoanController@confirmCard')->name('web-loan.confirmCard');

Route::middleware(['auth'])->group(function () {

	Route::get('borrow/awaiting', 'Web\BorrowerController@awaiting')->name('web-borrow.awaiting');
	Route::get('borrow/loan/{loan_slug}', 'Web\BorrowerController@loan')->name('web-borrow.profile');
	Route::post('borrow/update-validity', 'Web\BorrowerController@updateValidity')->name('web-borrow-update.validity');
	Route::post('loan/status', 'Web\LoanController@status')->name('web-loan.update');
	
	Route::get('loan/ready-to-payout', 'Web\LoanController@readyToPayout')->name('web-loan.readyToPayout');
	Route::post('loan/payout', 'Web\LoanController@payout')->name('web-loan.payout');
	Route::get('loan/loan-due-threee-days-from-now', 'Web\LoanController@loanDueThreeDaysFromNow')->name('web-loan.loan-due-threee-days-from-now');
	Route::get('loan/pay-back/{loan_slug}', 'Web\LoanController@payBackProfile')->name('web-borrow.pay-back-profile');

	Route::post('loan/transaction-store', 'Web\TransactionController@store')->name('web-transaction.store');
	Route::post('loan/rechargeCard', 'Web\LoanController@rechargeCard')->name('web-rechargeCard.store');

	Route::post('loan/rechargeCard', 'Web\LoanController@rechargeCard')->name('web-rechargeCard.store');
	Route::post('loan/loanTransactionCompleted', 'Web\LoanController@loanTransactionCompleted')->name('web-loanTransaction.completed');

});